# Yuzu Steamdeck Setup

As you know Yuzu got taken down by Nintendo but the package still exists even if it was teken down on Flathub, this is how you can install and run Yuzu and various other software with Distrobox on the Steam Deck. 

Please make sure to have at least Steam OS 3.5 installed, if not please update as distr3obox and podman come with the newer versions of Steam OS.

## Getting started

To Simplify things we are going to use Manjaro as they have the yuzu package in thier repos.
Just to be sure I even backed up the package to Archive.org, please go to [Install local Package](#install-local-yuzu-package)

```
distrobox-create --image docker.io/manjarolinux/base --additional-packages "systemd yay base-devel binutils wget" --init manjaro
```
Now we will enter the Manjaro container and update it 
```
distrobox enter manjaro
sudo pacman -Syu
```
Now we can install yuzu with a simple command and add it as a desktop shortcut on Steam OS
```
sudo pacman -S yuzu
distrobox-export --app yuzu
```

## Install local Yuzu Package

If by any case the package is down or removed we can still download the package and locally install it. 
https://archive.org/details/yuzu-1706-1-x86_64.pkg.tar

To do that just follow these steps and we will re-enter the distrobox comtainer, skip the first step if you are already inside

```
distrobox enter manjaro
wget https://archive.org/download/yuzu-1706-1-x86_64.pkg.tar/yuzu-1706-1-x86_64.pkg.tar.zst
sudo pacman -U yuzu-1706-1-x86_64.pkg.tar.zst
```

